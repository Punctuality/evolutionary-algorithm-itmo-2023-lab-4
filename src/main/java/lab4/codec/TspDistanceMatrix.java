package lab4.codec;

public record TspDistanceMatrix(int[][] distanceMatrix) {}
