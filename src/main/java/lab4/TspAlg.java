package lab4;

import lab4.codec.TspDescription;
import lab4.codec.TspReader;
import lab4.eval.CombinedTerminationCondition;
import lab4.evoalgo.*;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.GenerationCount;
import org.uncommons.watchmaker.framework.termination.TargetFitness;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;

public class TspAlg {

    public static void main(String[] args) throws FileNotFoundException {
        String problem = "XQF131"; // name of problem or path to input file

        int populationSize = 200; // size of population
        int generations = 50000; // number of generations

        Random random = new Random(); // random


        File tspFile = new File(String.format("src/main/resources/%s.tsp", problem.toLowerCase()));

        TspDescription description = TspReader.readDescription(tspFile);

        CandidateFactory<TspSolution> factory = new TspFactory(description.dimension); // generation of solutions

        ArrayList<EvolutionaryOperator<TspSolution>> operators = new ArrayList<EvolutionaryOperator<TspSolution>>();
        operators.add(new TspCrossover(0.1, 5)); // Crossover
        operators.add(new TspMutation(TspMutation.MutationType.INSERTION, 0.5)); // Mutation
        EvolutionPipeline<TspSolution> pipeline = new EvolutionPipeline<TspSolution>(operators);

        SelectionStrategy<Object> selection = new RouletteWheelSelection(); // Selection operator

        FitnessEvaluator<TspSolution> evaluator = new TspFitnessFunction(description); // Fitness function

        SteadyStateEvolutionEngine<TspSolution> algorithm = new SteadyStateEvolutionEngine<>(
                factory, pipeline, evaluator, selection, populationSize, false, random);


        algorithm.addEvolutionObserver(new EvolutionObserver() {
            public void populationUpdate(PopulationData populationData) {
                if (populationData.getGenerationNumber() % 100 == 0) {
                    double bestFit = populationData.getBestCandidateFitness();
                    System.out.println("Generation " + populationData.getGenerationNumber() + ": " + bestFit);
                    TspSolution best = (TspSolution) populationData.getBestCandidate();
                    System.out.println("\tBest solution = " + best.toString());
                }
            }
        });

        TerminationCondition terminate = new CombinedTerminationCondition(
                CombinedTerminationCondition.CombineType.OR,
                new GenerationCount(generations + 1),
                new TargetFitness(565.0, false)
        );
        long startTime = System.currentTimeMillis();
        algorithm.evolve(populationSize, 1, terminate);
        long endTime = System.currentTimeMillis();
        System.out.println("Time: " + (endTime - startTime) + " ms");
    }
}
