package lab4.codec.nodes;

public record TspNode2D(int id, int x, int y) implements TspNode {
    @Override
    public int getId() {
        return id;
    }
}
