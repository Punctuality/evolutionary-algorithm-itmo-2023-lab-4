package lab4.codec.nodes;

public record TspNode3D(int id, int x, int y, int z) implements TspNode {
    @Override
    public int getId() {
        return id;
    }
}
